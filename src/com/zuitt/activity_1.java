package com.zuitt;

import java.util.Scanner;

public class activity_1 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");
        int answer = 1;
        int counter = 1;

        // comment out either While or For loop

        // using While loop
//        try {
//            int num = in.nextInt();
//            while (num<0){
//                System.out.println("Cannot compute for factorials of negative numbers... Please input a new number.");
//                num = in.nextInt();
//            }
//            while(counter<=num){
//                answer = answer*counter;
//                counter++;
//            }
//            System.out.println("Factorial of " + num + " is: " + answer);
//        } catch (Exception e){
//            System.out.println("Invalid input, this program only accepts integer inputs.");
//            e.printStackTrace();
//        }

        // using For loop
        try {
            int num = in.nextInt();
            for(num = num; num<0; num = in.nextInt()){
                System.out.println("Cannot compute for factorials of negative numbers... Please input a new number.");
            }
            for(counter=1; counter<=num; counter++){
                answer = answer*counter;
            }
            System.out.println("Factorial of " + num + " is: " + answer);
        } catch (Exception e){
            System.out.println("Invalid input, this program only accepts integer inputs.");
            e.printStackTrace();
        }
    }
}
